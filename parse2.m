file = fopen('67.txt');
img_data = fscanf(file, '%d');
img_data = vec2mat(img_data, 640);
fclose(file);

file = fopen('67T.txt');
T_data = fscanf(file, '%d');
fclose(file);

file = fopen('../coefK_cor.txt');
K_cor = fscanf(file, '%d');
K_cor = vec2mat(K_cor, 640);
fclose(file);

file = fopen('../coefB_T.txt');
B_T = fscanf(file, '%d');
fclose(file);

file = fopen('../coefB.txt');
B_data = fscanf(file, '%d');
B_data = transpose(B_data(3:end));
B = permute(reshape(B_data, 640, 480, 38), [2 1 3]);

test = squeeze(B(:,:,1));

fclose(file);

clearvars B_data file;

image = zeros(480, 640);

for i = 1:480
    for B_T_Num = 1:38
        if T_data(i) <= B_T(B_T_Num)
            break;
        end
    end
    B_T_Num = B_T_Num - 1;
    for j = 1:640
        Koef = (T_data(i) - B_T(B_T_Num))./(B_T(B_T_Num+1) - B_T(B_T_Num));
        delta = B(i,j,B_T_Num+1)-B(i,j,B_T_Num);
        delK = (1-Koef).*delta;
        KK = B(i,j,B_T_Num+1) - delK;
        %img_data(i,j) - KK
        image(i,j) = (K_cor(i,j)).*(img_data(i,j) - KK)./32768 - 33000;
        if image(i,j) < 0
            image(i,j) = 0;
        end
    end
end

%min_im = min(min(image));

%for i = 1:480
%    for j = 1:640
%        image(i, j) = image(i,j) - min_im;
%    end
%end

image = mat2gray(image, [min(min(image)) max(max(image))]);


%figure;


figure
subplot(2,2,1), imshow(image);
subplot(2,2,2), imhist(image);   %Гистограмма
eqwImg = imadjust(image);%Коррекция динамического диапазона
%eqwImg = histeq(image(:,:,i));  %Эквализация
%figure
subplot(2,2,3), imshow(eqwImg);         %Новое изображения
subplot(2,2,4), imhist(eqwImg);         %Новая гистограмма
%figure, imshowpair(image(:,:,i), eqwImg, 'diff');


%figure, imshowpair(image(:,:,1), image(:,:,2), 'diff');

clearvars j i k ans;



